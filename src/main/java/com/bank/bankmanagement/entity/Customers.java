package com.bank.bankmanagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customers")
public class Customers {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "c_id")
	private int c_id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "age")
	private int age;

	@Column(name = "identity")
	private String identity;
	

	@Column(name = "address")
	private String address;
	
	@Column(name = "balance")
	private double balance;

	
	public int getC_id() {
		return c_id;
	}

	public void setC_id(int c_id) {
		this.c_id = c_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Customers(int c_id, String name, String email, String phone, int age, String identity, String address,
			double balance) {
		super();
		this.c_id = c_id;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.age = age;
		this.identity = identity;
		this.address = address;
		this.balance = balance;
	}

	public Customers() {
		super();

	}

	@Override
	public String toString() {
		return "Customers [c_id=" + c_id + ", name=" + name + ", email=" + email + ", phone=" + phone + ", age=" + age
				+ ", identity=" + identity + ", address=" + address + ", balance=" + balance + "]";
	}
	
	
	

}
