package com.bank.bankmanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transactions")
public class Transactions {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "trans_id")
	private int trans_id;
	
	@Column(name = "trans_date")
	private Date trans_date;
	
	@Column(name = "trans_amount")
	private double trans_amount;
	
	@Column(name = "from_cid")
	private int from_cid;
	
	@Column(name = "to_cid")
	private int to_cid;

	public int getTrans_id() {
		return trans_id;
	}

	public void setTrans_id(int trans_id) {
		this.trans_id = trans_id;
	}

	public Date getTrans_date() {
		return trans_date;
	}

	public void setTrans_date(Date trans_date) {
		this.trans_date = trans_date;
	}

	public double getTrans_amount() {
		return trans_amount;
	}

	public void setTrans_amount(double trans_amount) {
		this.trans_amount = trans_amount;
	}

	public int getFrom_cid() {
		return from_cid;
	}

	public void setFrom_cid(int from_cid) {
		this.from_cid = from_cid;
	}

	public int getTo_cid() {
		return to_cid;
	}

	public void setTo_cid(int to_cid) {
		this.to_cid = to_cid;
	}

	public Transactions(int trans_id, int c_id, Date trans_date, double trans_amount, int from_cid, int to_cid) {
		super();
		this.trans_id = trans_id;
		this.trans_date = trans_date;
		this.trans_amount = trans_amount;
		this.from_cid = from_cid;
		this.to_cid = to_cid;
	}

	public Transactions() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Transactions [trans_id=" + trans_id + ", trans_date=" + trans_date + ", trans_amount=" + trans_amount
				+ ", from_cid=" + from_cid + ", to_cid=" + to_cid + "]";
	}


	
}
