package com.bank.bankmanagement.service;

import java.util.List;

import com.bank.bankmanagement.entity.Customers;

public interface CustomerService {
	
	public List<Customers> getAllCustomers();
	
	public Customers getCustomerById(int custId);
	
    public Customers addOrUpdate(Customers customer);
	
	public Customers  deleteCustomer(int c_id)throws Exception;
	
}
