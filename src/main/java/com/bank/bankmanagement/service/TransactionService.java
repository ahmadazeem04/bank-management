package com.bank.bankmanagement.service;

import java.util.List;

import com.bank.bankmanagement.entity.Transactions;

public interface TransactionService {
	
	public List<Transactions> getAllTransaction();

	public Transactions getTransById(int serialId);

	public Transactions addTransaction(Transactions transaction) throws Exception;

}
