package com.bank.bankmanagement.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.bankmanagement.entity.Customers;
import com.bank.bankmanagement.entity.Transactions;
import com.bank.bankmanagement.repository.CustomerRepository;
import com.bank.bankmanagement.repository.TransactionRepository;
import com.bank.bankmanagement.service.TransactionService;
import com.bank.bankmanagement.exception.ResourceNotFoundException;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Override
	public List<Transactions> getAllTransaction() {

		return (List<Transactions>) transactionRepository.findAll();
	}

	@Override
	public Transactions getTransById(int serialId) {
		// TODO Auto-generated method stub
		return transactionRepository.findById(serialId).orElseThrow(()->new ResourceNotFoundException("Transaction number "+ serialId+" not found"));
	}

	@Override
	public Transactions addTransaction(Transactions transaction) throws Exception {
		

		int fromCId 			= transaction.getFrom_cid();
		int toCId 				= transaction.getTo_cid();
		double transAmount  	= transaction.getTrans_amount();
		
		Customers fromCustomer  = customerRepository.getById(fromCId);
		Customers toCustomer 	= customerRepository.getById(toCId);
		
		double fBal 			= fromCustomer.getBalance();
		
		if(fBal >= transAmount)
		{			
			//Updating balance of customer transferring the amount
			fromCustomer.setBalance(fBal-transAmount);
			customerRepository.save(fromCustomer);
			
			//Updating balance of customer receiving the amount
			toCustomer.setBalance(toCustomer.getBalance()+transAmount);
			customerRepository.save(toCustomer);
		}
		else 
		{
			throw new Exception("Insufficient Balance,transaction can't be performed! ");
		}
		
		return transactionRepository.save(transaction);
	}

}
