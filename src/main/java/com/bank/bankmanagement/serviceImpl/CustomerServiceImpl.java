package com.bank.bankmanagement.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.bankmanagement.entity.Customers;
import com.bank.bankmanagement.repository.CustomerRepository;
import com.bank.bankmanagement.service.CustomerService;
import com.bank.bankmanagement.exception.ResourceNotFoundException;

@Service
public class CustomerServiceImpl implements CustomerService{

	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public List<Customers> getAllCustomers() {

		return (List<Customers>) customerRepository.findAll();
	}

	@Override
	public Customers getCustomerById(int custId) {
		
		return customerRepository.findById(custId).orElseThrow(()->new ResourceNotFoundException("User not found"));
	}

	@Override
	public Customers addOrUpdate(Customers customer) {
		return customerRepository.save(customer);
	}

	@Override
	public Customers deleteCustomer(int c_id) throws Exception {
		Customers deletedCustomer = null;
		try {
			deletedCustomer = customerRepository.findById(c_id).orElse(null);
			if (deletedCustomer == null) {
				throw new Exception("Customer id "+c_id+" is not available in database");
			} else
				customerRepository.deleteById(c_id);

		} catch (Exception ex) {
			throw ex;
		}
		return deletedCustomer;
	}


}
