package com.bank.bankmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bank.bankmanagement.entity.Transactions;

public interface TransactionRepository extends JpaRepository<Transactions,Integer> {

}
