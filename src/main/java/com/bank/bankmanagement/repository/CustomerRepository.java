package com.bank.bankmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bank.bankmanagement.entity.Customers;

public interface CustomerRepository extends JpaRepository<Customers,Integer> {

}
