package com.bank.bankmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.bankmanagement.entity.Customers;
import com.bank.bankmanagement.entity.Transactions;
import com.bank.bankmanagement.service.CustomerService;
import com.bank.bankmanagement.service.TransactionService;


@RestController
@RequestMapping("/bank")
public class BankController {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private TransactionService transactionService;
	
	@GetMapping("/")
	public String home()
	{
		return "index";
	}
	
	@GetMapping("/allcustomers")
	public ResponseEntity<List<Customers>> getAllCustomers()
	{
		List<Customers> customers = null;
		try 
		{
			customers = customerService.getAllCustomers();
			
		}
		catch(Exception ex)
		{
			ex.getMessage();
		}
		return new ResponseEntity<List<Customers>>(customers,HttpStatus.OK);
	}
	@GetMapping("getById/{id}")
	@Cacheable(value = "Customer",key = "#custId" )
	public ResponseEntity<Customers> getCustomerById(@PathVariable("id") int custId)
	{
		Customers customer = null;
		
        customer = customerService.getCustomerById(custId);
		
		return new ResponseEntity<Customers>(customer, HttpStatus.OK);
	}
	
	@PostMapping("/addorupdate")
	public ResponseEntity<Customers> addOrUpdate(@RequestBody Customers cust) {

		Customers customer = null;

		try 
		{
			customer = customerService.addOrUpdate(cust);

		} catch (Exception ex) {
			ex.getMessage();
		}

		return new ResponseEntity<Customers>(customer, HttpStatus.OK);
	}
	
	@DeleteMapping("/deleteById/{id}")
	@CacheEvict(value = "Customer",allEntries = true)
	public ResponseEntity<Customers> deleteCustomer(@PathVariable("id") int c_id) {
		Customers customers = null;
		try {
			customers = customerService.deleteCustomer(c_id);
		} catch (Exception ex) {
			ex.getMessage();
		}

		return new ResponseEntity<Customers>(customers, HttpStatus.OK);
	}
	
	@GetMapping("/transactions")
	public ResponseEntity<List<Transactions>> getAllTransaction()
	{
		
		List<Transactions> transaction = null;

		try 
		{
			transaction =  transactionService.getAllTransaction();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return new ResponseEntity<List<Transactions>>(transaction, HttpStatus.OK);
		
	}
	
	@PostMapping("/addtransaction")
	public ResponseEntity<Transactions> addTransaction( @RequestBody Transactions transactions) {
		Transactions transaction = null;

		try 
		{
			transaction = transactionService.addTransaction(transactions);
		} 
		catch (Exception ex) {
			ex.getMessage();
		}
		return new ResponseEntity<Transactions>(transaction, HttpStatus.OK);
	}

}
